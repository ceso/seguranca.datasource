package br.jus.stf.seguranca.datasource;

public enum IdentificadorUsuarioEnum {
	LOGIN,
	EMAIL,
	CPF
}
