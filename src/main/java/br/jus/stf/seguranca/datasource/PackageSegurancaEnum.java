package br.jus.stf.seguranca.datasource;

public enum PackageSegurancaEnum {
	PRC_SEGURANCA_JBOSS,
	PRC_SEGURANCA_JBOSS_SIAA,
	PRC_SEGURANCA_EXTERNO,
	PRC_SEGURANCA_SIMPLES
}
